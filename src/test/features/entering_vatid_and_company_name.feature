@Web
Feature: Entering VAT ID and the company name
  In order to avoid errors
  As a Teddysoft employee
  I want to make sure the VAT ID and company name are consistency


  Scenario Outline: Entering VAT ID gets the corresponding company name
    When  I enter the VAT ID "<vatId>"
    Then  I should see the company name "<coName>"
    Examples:
      | vatId      | coName                                         | notes                                             |
      | 53909614 | 泰迪軟體科技有限公司                           | Normal company                                   |
      | 27932771 | 統一超商股份有限公司台南縣第一八三分公司     | Branch company, CoName come from 財政部         |
      | 28214192 | 頂饌國際股份有限公司宜蘭分公司                | Branch company, CoName come from searching again |
      | 28988153 | 英屬蓋曼群島商科嘉國際股份有限公司台灣分公司 | Foreign company                                   |
      | 11111111 | 東發視聽社                                      | Commercial registration                            |


  Scenario Outline: Entering wrong VAT ID format
    When  I enter the VAT ID "<vatId>"
    Then  I shouldn't see the company name
    Examples:
      | vatId      | notes                      |
      | 0          | Vat ID length less than 8    |
      | 12345      | Vat ID length less than 8    |
      | 123456789 | Vat Id length greater than 8 |
      | 2a85sd2f   | Include letters             |
      | \$^= @#?>  | Include symbols           |


  Scenario Outline: Entering company name gets the corresponding VAT ID
    When  I enter the company name "<coName>"
    Then  I should see the VAT ID "<vatId>"
    Examples:
      | coName               | vatId      | notes                           |
      | 泰迪軟體科技有限公司 | 53909614 | Normal company                |
      | 綠苑禮品行            | 01246636 | Commercial registration         |
      | 國立臺北科技大學      | 92021164 | Other class                     |
#      | 統一                   | 02428509 | Has multiple data, show first one |

  @LastOne
  Scenario: Entering company name that does not have any vat id
    When  I enter the company name "無此公司測試"
    Then  I shouldn't see the company vat id