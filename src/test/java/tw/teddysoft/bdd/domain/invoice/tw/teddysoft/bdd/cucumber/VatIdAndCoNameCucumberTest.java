package tw.teddysoft.bdd.domain.invoice.tw.teddysoft.bdd.cucumber;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by Lab1324 on 2017/4/15.
 */

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/features/entering_vatid_and_company_name.feature",
        glue = {"step"},
        format = {"json:target/cucumber-entering_vatid_and_company_name.json", "html:target/site/entering_vatid_and_company_name/cucumber-pretty"}
)
public class VatIdAndCoNameCucumberTest {
}
