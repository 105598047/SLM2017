package tw.teddysoft.bdd.domain.invoice;

import org.junit.Test;
import tw.teddysoft.bdd.domain.invoice.support.BusinessEntitySearcher;

import static org.junit.Assert.assertEquals;

/**
 * Created by Lab1324 on 2017/4/16.
 */
public class BusinessEntitySearcherTest {

    @Test
    public void getCoNameTest_VatId_string_is_empty() {
        assertEquals("", BusinessEntitySearcher.getCoName(""));
    }

    @Test
    public void getCoNameTest_the_VatId_format_is_illegal() {
        assertEquals("", BusinessEntitySearcher.getCoName("15955"));
    }

    @Test
    public void getCoNameTest_the_VatId_format_is_legal_and_CoName_is_exist() {
        assertEquals("蒂森電梯股份有限公司", BusinessEntitySearcher.getCoName("14070857"));
    }

    @Test
    public void getCoNameTest_the_VatId_format_is_legal_but_CoName_is_not_exist() {
        assertEquals("", BusinessEntitySearcher.getCoName("12345688"));
    }

    @Test
    public void getVatIdTest_CoName_string_is_empty() {
        assertEquals("", BusinessEntitySearcher.getVatID(""));
    }

    @Test
    public void getVatIdTest_CoName_is_not_complete_leading_to_multiple_results() {
        assertEquals("", BusinessEntitySearcher.getVatID("統一"));
    }

    @Test
    public void getVatIdTest_CoName_does_not_has_corresponding_VatId() {
        assertEquals("", BusinessEntitySearcher.getVatID("測試無此公司"));
    }

}
