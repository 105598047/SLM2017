package tw.teddysoft.bdd.domain.invoice;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Lab1324 on 2017/4/16.
 */
public class IssuingCompanyInvoiceTest {

    @Test
    public void issuing_company_invoice() {
        Invoice invoice = DefaultInvoiceBuilder.newInstance()
                .withVatRate(0.05)
                .withTaxIncludedPrice(36000)
                .withCoName("泰迪軟體科技有限公司")
                .withVatID("53909614")
                .issue();

        assertThat(invoice).isNotNull();
        assertThat(invoice.getVat()).isEqualTo(1714);
        assertThat(invoice.getTaxExcludedPrice()).isEqualTo(34286);
        assertThat(invoice.getVatId()).isEqualTo("53909614");
        assertThat(invoice.getCoName()).isEqualTo("泰迪軟體科技有限公司");
    }

    @Test
    public void issuing_company_invoice_only_with_company_name() {
        Invoice invoice = DefaultInvoiceBuilder.newInstance()
                .withVatRate(0.05)
                .withCoName("泰迪軟體科技有限公司")
                .withTaxIncludedPrice(36000)
                .issue();

        assertThat(invoice).isNotNull();
        assertThat(invoice.getVat()).isEqualTo(1714);
        assertThat(invoice.getTaxExcludedPrice()).isEqualTo(34286);
        assertThat(invoice.getVatId()).isEqualTo("53909614");
    }

    @Test
    public void issuing_company_invoice_only_with_company_vatid() {
        Invoice invoice = DefaultInvoiceBuilder.newInstance()
                .withVatRate(0.05)
                .withVatID("53909614")
                .withTaxIncludedPrice(36000)
                .issue();

        assertThat(invoice).isNotNull();
        assertThat(invoice.getVat()).isEqualTo(1714);
        assertThat(invoice.getTaxExcludedPrice()).isEqualTo(34286);
        assertThat(invoice.getCoName()).isEqualTo("泰迪軟體科技有限公司");
    }
}
