package tw.teddysoft.bdd.domain.invoice.support;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Lab1324 on 2017/4/15.
 */
public class WebSearchBusinessEntity {

    public void enterVatID(String vatId) {
        JavascriptExecutor je = (JavascriptExecutor) Utility.getWebDriver();
        je.executeScript("$('#vatId').val('"+ vatId +"')");
        je.executeScript(" $('#vatId').trigger('input')");
    }

    public void enterCoName(String coName) {
        JavascriptExecutor je = (JavascriptExecutor) Utility.getWebDriver();
        je.executeScript("$('#coName').val('"+ coName +"')");
        je.executeScript(" $('#coName').trigger('input')");
    }

    private void WaitUntilValueIsNotEmpty(String elementID) {
        WebElement element = Utility.getWebDriver().findElement(By.name(elementID));
        WebDriverWait wait = new WebDriverWait( Utility.getWebDriver(), 10);
        wait.until(ExpectedConditions.attributeToBeNotEmpty(element, "value"));
    }

    public String getVatID(boolean waitUntilValueIsNotEmpty) {
        if(waitUntilValueIsNotEmpty)
            WaitUntilValueIsNotEmpty("vatId");
        return Utility.getWebDriver().findElement(By.name("vatId")).getAttribute("value");

    }

    public String getCoName(boolean waitUntilValueIsNotEmpty) {
        if(waitUntilValueIsNotEmpty)
            WaitUntilValueIsNotEmpty("coName");
        return  Utility.getWebDriver().findElement(By.name("coName")).getAttribute("value");
    }

}
