package tw.teddysoft.bdd.domain.invoice.support;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import tw.teddysoft.bdd.domain.invoice.Invoice;
import tw.teddysoft.bdd.domain.invoice.InvoiceBuilder;

/**
 * Created by teddy on 2017/3/30.
 */
public class WebInvoiceBuilder implements InvoiceBuilder {

    public WebInvoiceBuilder(){
    }

    private WebDriver getWebDriver(){
        return Utility.getWebDriver();
    }

    @Override
    public InvoiceBuilder withVatRate(double vatRate) {
        return this;
    }

    @Override
    public InvoiceBuilder withTaxIncludedPrice(int taxIncludedPrice) {
        getWebDriver().findElement(By.name("taxIncludedPrice")).sendKeys(String.valueOf(taxIncludedPrice));
        return this;
    }


    @Override
    public InvoiceBuilder withTaxExcludedPrice(Integer taxExcludedPrice) {
        getWebDriver().findElement(By.name("taxExcludedPrice")).sendKeys(String.valueOf(taxExcludedPrice));
        return this;
    }

    @Override
    public InvoiceBuilder withVatID(String vatId) {
        getWebDriver().findElement(By.name("vatId")).sendKeys(Keys.chord(Keys.CONTROL, "a"), vatId);
        return this;
    }

    @Override
    public InvoiceBuilder withCoName(String coName) {
        getWebDriver().findElement(By.name("coName")).sendKeys(Keys.chord(Keys.CONTROL, "a"), coName);
        return this;
    }

    @Override
    public Invoice issue() {

        getWebDriver().findElement(By.name("issue")).click();
        int vat = Integer.valueOf(getWebDriver().findElement(By.name("vat")).getAttribute("value"));
        int taxExcludedPrice = Integer.valueOf(getWebDriver().findElement(By.name("taxExcludedPrice")).getAttribute("value"));
        int taxIncludedPrice = Integer.valueOf(getWebDriver().findElement(By.name("taxIncludedPrice")).getAttribute("value"));
        String vatId = getWebDriver().findElement(By.name("vatId")).getAttribute("value");
        String coName = getWebDriver().findElement(By.name("coName")).getAttribute("value");
        double vatRate = 0.05;
        return new Invoice(taxIncludedPrice, vatRate, taxExcludedPrice, vat, vatId, coName);

    }

}
