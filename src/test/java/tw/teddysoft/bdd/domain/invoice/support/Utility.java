package tw.teddysoft.bdd.domain.invoice.support;

import cucumber.api.Scenario;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Properties;

/**
 * Created by teddy on 2017/3/26.
 */
public class Utility {

    private static WebDriver driver;

    public static final String CUCUMBER_PROPERTY_FILE = "./src/test/resources/cucumber.properties";

    static Logger logger = LoggerFactory.getLogger(Utility.class);

    public static boolean isUnderInvoiceWebMode(){
        Properties prop = Utility.loadProperties(Utility.CUCUMBER_PROPERTY_FILE);
        if("web".equals(prop.getProperty("invoice.ui"))) {
            return true;
        } else {
            return false;
        }
    }

    public static WebDriver getWebDriver() {
        if (null == driver) {
            System.setProperty("webdriver.chrome.driver" ,  "chromedriver.exe");
            driver = new ChromeDriver();
            driver.get("http://localhost:4567/invoice");
        }
        return driver;
    }

    public static void refreshPage() {
        if(driver != null)
            driver.get("http://localhost:4567/invoice");
    }

    public static void closeWebDriver() {
        if(driver != null) {
            driver.quit();
            driver = null;
        }
    }

    public static boolean isLastOne(Scenario scenario) {

        ArrayList tags = new ArrayList<>(scenario.getSourceTagNames());

        tags.remove("@Web");
        tags.remove("@LastOne");

        if(tags.size() == 0) return true;

        int exampleTotal = Integer.valueOf(tags.get(0).toString().replace("@", "")) + 1;

        int exampleNowNum = Integer.valueOf(scenario.getId().split(";;")[1]);

        return exampleTotal == exampleNowNum;

    }

    public static Properties loadProperties(String fileName)  {
        InputStream input = null;
        Properties prop = new Properties();

        try {
            input = new FileInputStream(fileName);
            prop.load(input);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        finally {
            if(null != input)
                try {
                    input.close();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
        }
        return prop;
    }

    public static void sleep(int ms){
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
