package step;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tw.teddysoft.bdd.domain.invoice.support.Utility;
import tw.teddysoft.bdd.web.app.InvoiceWeb;

public class Hook {

    Logger logger = LoggerFactory.getLogger(Hook.class);

    private static boolean serverOpening = false;

    @Before
    public void beforeScenario(Scenario scenario){
         if (Utility.isUnderInvoiceWebMode() && !serverOpening) {
             InvoiceWeb.main(new String[0]);
             serverOpening = true;
         }
    }

    @Before("@Web")
    public void beforeScenario_NeedOpenServer() {
        if(!serverOpening) {
            InvoiceWeb.main(new String[0]);
            serverOpening = true;
        }
    }

    @After //After each scenario
    public void afterScenario(Scenario scenario){
            Utility.refreshPage();
    }

    @After("@LastOne")
    public void afterLastOneScenario(Scenario scenario) {
        if(Utility.isLastOne(scenario))
            Utility.closeWebDriver();
    }

}
