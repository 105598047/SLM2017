package step;

import cucumber.api.java8.En;
import tw.teddysoft.bdd.domain.invoice.support.WebSearchBusinessEntity;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static tw.teddysoft.bdd.domain.invoice.support.Utility.sleep;

/**
 * Created by Lab1324 on 2017/4/12.
 */
public class SearchBusinessEntityStep implements En {

    private WebSearchBusinessEntity webDriver = new WebSearchBusinessEntity() ;

    public SearchBusinessEntityStep() {

        When("^I enter the VAT ID \"([^\"]*)\"$", (String vatId) -> {
            webDriver.enterVatID(vatId.replaceAll("\\s+",""));
        });

        Then("^I should see the company name \"([^\"]*)\"$", (String coName) -> {
            assertThat(webDriver.getCoName(true), is(coName));
        });

        When("^I enter the company name \"([^\"]*)\"$", (String coName) -> {
            webDriver.enterCoName(coName.replaceAll("\\s+",""));
        });

        Then("^I should see the VAT ID \"([^\"]*)\"$", (String vatId) -> {
            assertThat(webDriver.getVatID(true), is(vatId));
        });

        Then("^I shouldn't see the company name$", () -> {
            sleep(1000);
            assertThat(webDriver.getCoName(false), is(""));
        });

        Then("^I shouldn't see the company vat id$", () -> {
            sleep(1000);
            assertThat(webDriver.getVatID(false), is(""));
        });

    }

}
