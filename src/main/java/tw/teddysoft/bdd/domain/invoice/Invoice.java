package tw.teddysoft.bdd.domain.invoice;

/**
 * Created by teddy on 2017/3/2.
 */
public class Invoice {
    private final int taxExcludedPrice;
    private final double vatRate;
    private final int taxIncludedPrice;
    private final int vat;
    private String vatId;
    private String coName;

//    public Invoice(int taxIncludedPrice, double vatRate, int taxExcludedPrice, int vat) {
//        this.taxIncludedPrice = taxIncludedPrice;
//        this.vatRate = vatRate;
//        this.taxExcludedPrice = taxExcludedPrice;
//        this.vat = vat;
//    }

    public Invoice(int taxIncludedPrice, double vatRate, int taxExcludedPrice, int vat, String vatId, String coName) {
        this.taxIncludedPrice = taxIncludedPrice;
        this.vatRate = vatRate;
        this.taxExcludedPrice = taxExcludedPrice;
        this.vat = vat;
        this.vatId = vatId;
        this.coName = coName;
    }

    public int getTaxIncludedPrice() {
        return taxIncludedPrice;
    }

    public int getVat(){return this.vat; }

    public double getVatRate() {
        return vatRate;
    }

    public int getTaxExcludedPrice()
    {
        return this.taxExcludedPrice;
    }

    public String getVatId() {
        return this.vatId;
    }

    public String getCoName() {
        return this.coName;
    }
}
