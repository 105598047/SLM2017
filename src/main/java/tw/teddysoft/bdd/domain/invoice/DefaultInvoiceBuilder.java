package tw.teddysoft.bdd.domain.invoice;

import tw.teddysoft.bdd.domain.invoice.support.BusinessEntitySearcher;

/**
 * Created by teddy on 2017/3/9.
 */
public class DefaultInvoiceBuilder implements InvoiceBuilder {

    private double vatRate = 0.0;
    private int taxIncludedPrice = 0;
    private int taxExcludedPrice = 0;
    private String vatId = "";
    private String coName = "";

    public DefaultInvoiceBuilder() {};

    public static DefaultInvoiceBuilder newInstance(){
        return new DefaultInvoiceBuilder();
    }

    @Override
    public DefaultInvoiceBuilder withVatRate(double vatRate) {
        this.vatRate = vatRate;
        return this;
    }

    @Override
    public DefaultInvoiceBuilder withTaxIncludedPrice(int taxIncludedPrice) {
        if(taxIncludedPrice != 0) {
            this.taxIncludedPrice = taxIncludedPrice;
            this.taxExcludedPrice = 0;
        }
        return this;
    }

    @Override
    public Invoice issue() {

        if(isUseTaxExcludedPriceToCalculateInvoice()) {
            taxIncludedPrice =(int) Math.round(taxExcludedPrice * (1 + vatRate));
        }

        if(isNeedToSearchVatId())
            coName = BusinessEntitySearcher.getCoName(vatId);

        if(isNeedToSearchCoName())
            vatId = BusinessEntitySearcher.getVatID(coName);

//        if((!vatId.isEmpty()) && (!coName.isEmpty()))
//            return new Invoice( taxIncludedPrice, vatRate,
//                                InvoiceCalculator.getTaxExcludedPrice(taxIncludedPrice, vatRate),
//                                InvoiceCalculator.getVAT(taxIncludedPrice, vatRate),
//                                vatId, coName);
//        else return new Invoice( taxIncludedPrice, vatRate,
//                            InvoiceCalculator.getTaxExcludedPrice(taxIncludedPrice, vatRate),
//                            InvoiceCalculator.getVAT(taxIncludedPrice, vatRate));

        return new Invoice( taxIncludedPrice, vatRate,
                            InvoiceCalculator.getTaxExcludedPrice(taxIncludedPrice, vatRate),
                            InvoiceCalculator.getVAT(taxIncludedPrice, vatRate),
                            vatId, coName);
    }

    @Override
    public DefaultInvoiceBuilder withTaxExcludedPrice(Integer taxExcludedPrice) {
        if(taxExcludedPrice != 0) {
            this.taxExcludedPrice = taxExcludedPrice;
            taxIncludedPrice = 0;
        }
        return this;
    }

    @Override
    public InvoiceBuilder withVatID(String vatId) {
        this.vatId = vatId;
        return this;
    }

    @Override
    public InvoiceBuilder withCoName(String coName) {
        this.coName = coName;
        return this;
    }

    private boolean isUseTaxExcludedPriceToCalculateInvoice(){
        return (0 == taxIncludedPrice && 0 != taxExcludedPrice);
    }

    private boolean isNeedToSearchVatId() {
        return (!vatId.isEmpty()) && coName.isEmpty();
    }

    private boolean isNeedToSearchCoName() {
        return vatId.isEmpty() && (!coName.isEmpty());
    }

}
