package tw.teddysoft.bdd.domain.invoice.support;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by Lab1324 on 2017/4/15.
 */


public class BusinessEntitySearcher {

    private static Logger logger = LoggerFactory.getLogger(BusinessEntitySearcher.class);

    public static String getCoName(String vatId) {

        if(vatId.isEmpty()) return "";

        try {

//            URL url = new URL("https://company.g0v.ronny.tw/api/show/" + String.valueOf(vatId));
//            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//            conn.setRequestMethod("GET");
//            conn.setRequestProperty("Accept", "application/json");
//
//            if (conn.getResponseCode() != 200) {
//                throw new RuntimeException("Failed : HTTP error code : "
//                        + conn.getResponseCode());
//            }
//
//            BufferedReader br = new BufferedReader(new InputStreamReader(
//                    (conn.getInputStream())));

//            JsonObject jsonObject = new JsonParser().parse(decode(br.readLine())).getAsJsonObject();
//            conn.disconnect();

            JsonObject result = httpGET("https://company.g0v.ronny.tw/api/show/" + String.valueOf(vatId));

            logger.info(result.toString());

            if(findTheCompany(result))
                return getCompanyName(result.getAsJsonObject("data"));


        } catch (Exception e) {
            logger.info(e.getMessage());
        }

        return "";

    }

    public static String getVatID(String coName) {

        try {

            JsonObject result = httpGET("https://company.g0v.ronny.tw/api/search?q=" +URLEncoder.encode(coName, "UTF-8"));

            logger.info(result.toString());

            if(findTheVatID(result)) {
                String vatId = result.getAsJsonArray("data").get(0).getAsJsonObject().get("統一編號").toString();
                return vatId.replace("\"", "");
            }

        } catch (Exception e) {
            logger.info(e.getMessage());
        }

        return "";

    }


    private static String decode(String s) {
        StringBuilder sb = new StringBuilder(s.length());
        char[] chars = s.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            char c = chars[i];
            if (c == '\\' && chars[i + 1] == 'u') {
                char cc = 0;
                for (int j = 0; j < 4; j++) {
                    char ch = Character.toLowerCase(chars[i + 2 + j]);
                    if ('0' <= ch && ch <= '9' || 'a' <= ch && ch <= 'f') {
                        cc |= (Character.digit(ch, 16) << (3 - j) * 4);
                    } else {
                        cc = 0;
                        break;
                    }
                }
                if (cc > 0) {
                    i += 5;
                    sb.append(cc);
                    continue;
                }
            }
            sb.append(c);
        }
        return sb.toString();
    }

    private static JsonObject httpGET(String url) throws Exception{

        URL _url = new URL(url);
        HttpURLConnection conn = (HttpURLConnection) _url.openConnection();
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Accept", "application/json");

        if (conn.getResponseCode() != 200) {
            throw new RuntimeException("Failed : HTTP error code : "
                    + conn.getResponseCode());
        }

        BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

        JsonObject jsonObject = new JsonParser().parse(decode(br.readLine())).getAsJsonObject();

        conn.disconnect();

        return jsonObject;

    }

    private static String getCompanyName(JsonObject result) {

        if(result.has("公司名稱") && (!result.get("公司名稱").isJsonArray()))
            return result.get("公司名稱").getAsString();

        if(result.has("分公司名稱")) {
            if(result.getAsJsonObject("財政部").toString().compareTo("{}") != 0)
                return result.getAsJsonObject("財政部").get("營業人名稱").getAsString();
            else {
                String parentCoName = getCoName(result.get("總(本)公司統一編號").getAsString());
                return parentCoName + result.get("分公司名稱").getAsString();
            }
        }

        if(result.has("商業名稱"))
            return result.get("商業名稱").getAsString();

        if(result.has("名稱"))
            return result.get("名稱").getAsString();

        return "";

    }

    private static boolean findTheCompany(JsonObject result) {
        return (!result.has("error"));
    }

    private static boolean findTheVatID(JsonObject result) {
        return result.getAsJsonArray("data").size() == 1;
    }

//    private static boolean isNormalCompany(JsonObject result) {
//        return result.has("公司名稱") && (!result.get("公司名稱").isJsonArray());
//    }
//
//    private static boolean isBranchCompany(JsonObject result) {
//        return result.has("分公司名稱");
//    }
//
//    private static boolean isCommercialRegistration(JsonObject result) {
//        return result.has("商業名稱");
//    }

}
