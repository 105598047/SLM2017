package tw.teddysoft.bdd.web.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.*;
import spark.template.velocity.VelocityTemplateEngine;
import tw.teddysoft.bdd.domain.invoice.Invoice;
import tw.teddysoft.bdd.domain.invoice.InvoiceBuilder;
import tw.teddysoft.bdd.domain.invoice.support.BusinessEntitySearcher;

import java.util.HashMap;
import java.util.Map;

import static spark.Spark.*;


/**
 * Created by teddy on 2017/3/2.
 */

/**
 * VelocityTemplateRoute example.
 */
public final class InvoiceWeb {

    static Logger logger = LoggerFactory.getLogger(InvoiceWeb.class);

    public static void main(String[] args) {

        int portNumber = 4567;

        if(args.length > 0) portNumber = Integer.valueOf(args[0]);

        port(portNumber);

        logger.info("Set listening port: " + portNumber);

        get("/invoice", (request, response) -> {
            Map<String, Object> model = new HashMap<>();
            model.put("Title", "三聯式發票");

            return new ModelAndView(model, "invoice_input.vm"); // located in the resources directory
        }, new VelocityTemplateEngine());

        get("/invoice/api/searchCoName/:id", (request, response) -> BusinessEntitySearcher.getCoName(request.params(":id")));

        get("/invoice/api/searchVarID/:name", (request, response) -> BusinessEntitySearcher.getVatID(request.params("name")));

        post("/invoice", (request, response) -> {
            Invoice invoice;
            int taxIncludedPrice = integerValue(request.queryMap("taxIncludedPrice").value());
            int taxExcludedPrice = integerValue(request.queryMap("taxExcludedPrice").value());
            double vatRate = request.queryMap("vatRate").doubleValue();
            String vatId = request.queryMap("vatId").value();
            String coName = request.queryMap("coName").value();

            invoice = InvoiceBuilder.newInstance().
                    withTaxIncludedPrice(taxIncludedPrice).
                    withTaxExcludedPrice(taxExcludedPrice).
                    withVatRate(vatRate).
                    withVatID(vatId).
                    withCoName(coName).
                    issue();

//            if (isUseTaxIncludedPriceToCalculateInvoice(taxIncludedPrice)) {
//                invoice = InvoiceBuilder.newInstance().
//                        withTaxIncludedPrice(taxIncludedPrice).
//                        withVatRate(vatRate).
//                        issue();
//            }
//            else {
//                invoice = InvoiceBuilder.newInstance().
//                        withTaxExcludedPrice(taxExcludedPrice).
//                        withVatRate(vatRate).
//                        issue();
//            }

            Map<String, Object> model = new HashMap<>();
            model.put("invoice", invoice);

            return new ModelAndView(model, "invoice_result.vm"); // located in the resources directory
        }, new VelocityTemplateEngine());

    }

//    private static boolean isUseTaxIncludedPriceToCalculateInvoice(int taxIncludedPrice){
//        return !(0 == taxIncludedPrice);
//    }

    private static int integerValue(String str){
        if ((null == str) || ("".equals(str)))
            return 0;
        else
           return Integer.valueOf(str);
    }


}